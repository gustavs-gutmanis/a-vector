var galleryData = [
	{name: "mini", size: 6}
	,{name: "big", size: 5}
	,{name: "avaliable-equipment", size: 6}
	,{name: "lifter", size: 5}
	//,{name: "logistics", size: 0}
]

$(function(){
	var ul = $("#equipment > ul");
	var pager = $("#equipment > div");

	for (var i = 0; i < galleryData.length; i++)
	{
		var data = galleryData[i];

		var li = $("<li />")
			.attr("style", "background-image: url(/img/gallery/" + data.name + "/bg.jpg)");

		var div = $("<div />")
			.addClass("gallery-wrapper");
		var innerUl = $("<ul />");

		for (var y = 1; y <= data.size; y++)
		{
			innerUl.append('<li><a href="/img/gallery/'+data.name+'/'+y+'.jpg" style="background-image: url(/img/gallery/'+data.name+'/'+y+'.jpg)"></a></li>');
		}

		var innerDiv = $("<div />");
		if (!data.size)
		{
			innerDiv.addClass("alone");
		}
		else
		{
			div.append(innerUl);
		}

		innerDiv.append('<h3 data-i18n="[html]gallery.'+data.name+'.title"></h3>');
		innerDiv.append('<p data-i18n="[html]gallery.'+data.name+'.content"></p>');

		div.append(innerDiv);
		li.append('<div class="overlay"></div>');
		li.append(div);
		ul.append(li);

		pager.append('<a href="javascript:;" class="'+(i==0?'active':"")+'"><span style="background-image: url(/img/gallery/'+data.name+'/thumb.jpg);"></span></a>');
	}

	$("#equipment > div > a").on({
		click: function(){
			var self = $(this);
			var index = self.index();

			var li = ul.children(":eq(" + index + ")");
			var newPos = -(li.width() * index);

			//transform: $args;
			//-ms-transform: $args;
			//-webkit-transform: $args;

			ul.animate({
				left: newPos
			}, 500);
			//ul.attr("style","left: "+newPos+"px;");

			self.addClass("active")
				.siblings(".active")
				.removeClass("active");
		}
	});

	//$("#equipment > ul > li > div > ul > li > a").magnificPopup({
	//	type: "image"
	//	, gallery: {
	//		enabled: true
	//	}
	//});
	//

	$("#equipment > ul > li").each(function(){
		$(this).find("div > ul > li > a").magnificPopup({
			type: "image"
			, gallery: {
				enabled: true
			}
		});
	});


});