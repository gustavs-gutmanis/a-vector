$(function(){
	$("#lang-wrapper > ul > li").on({
		click: function(event)
		{
			var self = $(this);
			if (self.hasClass("selected"))
			{
				return false;
			}

			var lang = self.attr("rel");
			$.i18n.setLng(lang);
			translateAll();

			$.cookie("lang", lang, {expires: 360, path: '/'});

			var isLeft = self.hasClass("left");
			var className = isLeft ? "left" : "right";
			self.siblings(".selected").removeClass("selected").addClass(className);
			self.removeClass(className).addClass("selected");

			return false;
		}
	});


	var currentLang = $.cookie("lang") ? $.cookie("lang") : "lv";
	var leftAssigned = false;
	$("#lang-wrapper > ul > li").each(function(){
		var self = $(this);
		var lang = self.attr("rel");
		if (lang == currentLang)
		{
			self.addClass("selected").removeClass("left").removeClass("right");
		}
		else if (!leftAssigned)
		{
			self.addClass("left").removeClass("right").removeClass("selected");
			leftAssigned = true;
		}
		else{
			self.addClass("right").removeClass("left").removeClass("selected");
		}
	});

	$("#lang-wrapper > ul").delay(100).fadeIn(1500);

	$(".no-touch .iconlist > li").on({
		mouseenter: function()
		{
			var self = $(this);
			self.children(".container").addClass("flipped");
		}
		, mouseleave: function()
		{
			var self = $(this);
			self.children(".container").removeClass("flipped");
		}
	});


	$(".touch .iconlist > li").on({
		click: function()
		{
			var self = $(this);
			self.children(".container").toggleClass("flipped");
		}
	});

	$.i18n.init({
		lng: currentLang
		//, debug: true
		, preload:['lv','en','ru']
		, fallbackLng: 'lv'
		, resGetPath: 'locales/__ns__.__lng__.json'
	}, function(){
		translateAll();
		$(".iconlist > li > .container > span").perfectScrollbar({
			suppressScrollX: true
			,includePadding: true
			,scrollYMarginOffset: 10
		});
		$("#equipment > ul > li > div > div > p").perfectScrollbar({
			suppressScrollX: true
			,includePadding: true
			,scrollYMarginOffset: 10
		});
		$("#main-wrapper").perfectScrollbar({});
	});

	$("#works > ul > li > a").magnificPopup({
		type: "image"
	});

	$("#main-wrapper").scroll(function(){
		var nav = $("#nav-wrapper");
		var top = $("#top");
		var triggerOffset = top.height();
		var currentOffset = Math.abs($("#sub-wrapper").offset().top);

		if (currentOffset > triggerOffset)
		{
			$("#nav-phone").addClass("shown");
		}
		else{
			$("#nav-phone").removeClass("shown");
		}
	});

	var videoTag = '<video autoplay loop preload="auto" poster="/video/loop.jpg" id="video"><source src="/video/loop.mp4" type="video/mp4"></video>';
	if (!isMobile.any)
	{
		$("#video-wrapper").append(videoTag);
	}
});

function translateAll()
{
	$("*[data-i18n]").i18n();
	setTimeout(function(){
		$(".iconlist > li > .container > span").perfectScrollbar("update");
		$("#equipment > ul > li > div > div > p").perfectScrollbar("update");
		$("#main-wrapper").perfectScrollbar("update");
	}, 200);
}